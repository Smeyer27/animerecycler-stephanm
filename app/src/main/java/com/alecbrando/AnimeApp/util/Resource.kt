package com.alecbrando.AnimeApp.util

sealed class Resource<T>(data: T? = null, errorMsg :String?) {
    data class Success<T>(val data: T) : Resource<T>(data, null)
    class Loading<T> : Resource<T>(null, null)
    data class Error<T>(val errorMsg: String) : Resource<T>(null, errorMsg)
}

