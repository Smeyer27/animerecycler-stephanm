package com.alecbrando.AnimeApp.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.alecbrando.AnimeApp.model.models.AnimeData
import com.alecbrando.AnimeApp.model.repo.RepoRepo
import com.alecbrando.AnimeApp.util.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class AnimeDetailViewModel: ViewModel() {

    private val repo by lazy { RepoRepo }

    private val _anime = MutableStateFlow<Resource<List<AnimeData>>>(Resource.Loading())
    val anime: StateFlow<Resource<List<AnimeData>>> get() = _anime

    fun getAnimeDetailsById(id: String){
        viewModelScope.launch(Dispatchers.Main) {
            repo.getAnimeById(id)?.let { _anime.emit(it) }
        }
    }
}