package com.alecbrando.AnimeApp.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.alecbrando.AnimeApp.model.models.AnimeData
import com.alecbrando.AnimeApp.model.repo.AnimeRepoImpl
import com.alecbrando.AnimeApp.util.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class AnimeListViewModel (private val repoImpl : AnimeRepoImpl): ViewModel() {

    private val _anime = MutableStateFlow<Resource<List<AnimeData>>>(Resource.Loading())
    val anime: StateFlow<Resource<List<AnimeData>>> get() = _anime

    fun getAnime() {
        viewModelScope.launch(Dispatchers.Main) {
            repoImpl.getAnime()?.let { _anime.emit(it) }
        }
    }

    fun getAnimeByID(id : String) {
        viewModelScope.launch(Dispatchers.Main) {
            repoImpl.getAnimeByID(id)?.let { _anime.emit(it) }
        }
    }
}