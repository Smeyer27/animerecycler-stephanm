package com.alecbrando.AnimeApp.viewmodels.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.alecbrando.AnimeApp.model.repo.AnimeRepoImpl
import com.alecbrando.AnimeApp.viewmodels.AnimeListViewModel

class AnimeViewModelFactory (private val repoImpl: AnimeRepoImpl) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return AnimeListViewModel(repoImpl) as T
    }
}

