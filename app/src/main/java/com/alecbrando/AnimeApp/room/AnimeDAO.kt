package com.alecbrando.AnimeApp.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.alecbrando.AnimeApp.model.models.AnimeData
import com.alecbrando.AnimeApp.model.models.Data

@Dao
interface AnimeDAO {

    @Query("SELECT*FROM its_anime")
    fun getAnimes() : List<AnimeData>
}