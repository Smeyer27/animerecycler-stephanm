package com.alecbrando.AnimeApp.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.alecbrando.AnimeApp.model.models.AnimeData
import com.alecbrando.AnimeApp.model.models.Animes
import com.alecbrando.AnimeApp.model.models.Data

@Database(entities = [AnimeData::class], exportSchema = false, version = 9000)
abstract class AnimeDB : RoomDatabase() {

    abstract fun animeDao() : AnimeDAO

    companion object {
        private const val DB_NAME = "ANIME_DATABASE"
        fun getDatabaseInstance(context: Context) : AnimeDB {
            return Room.databaseBuilder(context, AnimeDB::class.java, DB_NAME)
                .fallbackToDestructiveMigration().build()
        }
    }
}