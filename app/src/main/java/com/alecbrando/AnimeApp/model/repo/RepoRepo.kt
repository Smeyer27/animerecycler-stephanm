package com.alecbrando.AnimeApp.model.repo

import com.alecbrando.AnimeApp.model.AnimeApi
import com.alecbrando.AnimeApp.util.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.lang.Exception

object RepoRepo {

    private val fanService by lazy { AnimeApi.animeRetrofit }

    suspend fun getAnime() = withContext(Dispatchers.IO){
        return@withContext try{
            val response = fanService.getAnime()
            if(response.isSuccessful){
                Resource.Success(response.body()!!.getAnimeList)
            }else{
                Resource.Error("Something went Wrong! D:")
            }
        } catch (e: Exception){
            e.localizedMessage?.let { Resource.Error(it) }
        }
    }
    suspend fun getAnimeById(id : String) = withContext(Dispatchers.IO){
        return@withContext try{
            val response = fanService.getAnimeById(id)
            if(response.isSuccessful){
                Resource.Success(response.body()!!.getAnimeList)
            }else{
                Resource.Error("Something went Wrong! D:")
            }
        } catch (e: Exception){
            e.localizedMessage?.let { Resource.Error(it) }
        }
    }
}