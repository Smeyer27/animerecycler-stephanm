package com.alecbrando.AnimeApp.model.models

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Animes(
    val `data`: List<Data>
): Parcelable