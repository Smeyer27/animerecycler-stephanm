package com.alecbrando.AnimeApp.model.repo

import com.alecbrando.AnimeApp.model.AnimeApi
import com.alecbrando.AnimeApp.model.models.AnimeData
import com.alecbrando.AnimeApp.util.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.lang.Exception

interface AnimeRepoInt {
    suspend fun getAnime(): Resource<List<AnimeData>>?

    suspend fun getAnimeById(animeId: String): Resource<List<AnimeData>>?

}
