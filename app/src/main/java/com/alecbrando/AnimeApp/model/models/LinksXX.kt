package com.alecbrando.AnimeApp.model.models

data class LinksXX(
    val related: String,
    val self: String
)