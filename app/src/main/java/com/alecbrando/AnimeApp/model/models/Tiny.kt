package com.alecbrando.AnimeApp.model.models

data class Tiny(
    val height: Int,
    val width: Int
)