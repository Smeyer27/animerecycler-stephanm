package com.alecbrando.AnimeApp.model.models

data class SmallX(
    val height: Int,
    val width: Int
)