package com.alecbrando.AnimeApp.model.models

data class Dimensions(
    val large: Large,
    val small: Small,
    val tiny: Tiny
)