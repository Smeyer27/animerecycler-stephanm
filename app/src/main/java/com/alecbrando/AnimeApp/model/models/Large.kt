package com.alecbrando.AnimeApp.model.models

data class Large(
    val height: Int,
    val width: Int
)