package com.alecbrando.AnimeApp.model.models

data class LargeX(
    val height: Int,
    val width: Int
)