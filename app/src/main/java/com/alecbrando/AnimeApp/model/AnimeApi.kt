package com.alecbrando.AnimeApp.model

import com.alecbrando.AnimeApp.model.models.AnimeData
import com.alecbrando.AnimeApp.model.models.Data
import com.google.gson.annotations.SerializedName
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface AnimeApi {

    data class GetResponse(
        @SerializedName("data")
        val getAnimeList : List<AnimeData>
    )

    @GET("anime")
    suspend fun getAnime() : Response<GetResponse>

    @GET("anime")
    suspend fun getAnimeById(@Query("filter[id]") id: String): Response<GetResponse>


    companion object {
        val animeRetrofit by lazy {
            Retrofit
                .Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("https://kitsu.io/api/edge/")
                .build()
                .create(AnimeApi::class.java)
        }
    }
}
