package com.alecbrando.AnimeApp.model.models

data class Medium(
    val height: Int,
    val width: Int
)