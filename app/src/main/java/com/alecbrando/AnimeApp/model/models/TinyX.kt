package com.alecbrando.AnimeApp.model.models

data class TinyX(
    val height: Int,
    val width: Int
)