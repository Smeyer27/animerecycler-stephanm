package com.alecbrando.AnimeApp.model.models

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize


@Parcelize
@Entity(tableName = "its_anime")
data class AnimeData(
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    val animeId: String = "",
    val animeTitle: String = "",
    val animePosterImg: String = ""
): Parcelable
