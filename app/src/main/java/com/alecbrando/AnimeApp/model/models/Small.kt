package com.alecbrando.AnimeApp.model.models

data class Small(
    val height: Int,
    val width: Int
)