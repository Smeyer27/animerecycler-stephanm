package com.alecbrando.AnimeApp.model.models

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class CoverImage(
    val large: String = "",
    val original: String = "",
    val small: String = "",
    val tiny: String = ""
): Parcelable