package com.alecbrando.AnimeApp.model.repo

import android.content.Context
import com.alecbrando.AnimeApp.model.AnimeApi
import com.alecbrando.AnimeApp.model.models.AnimeData
import com.alecbrando.AnimeApp.model.models.Data
import com.alecbrando.AnimeApp.room.AnimeDB
import com.alecbrando.AnimeApp.util.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.lang.Exception

class AnimeRepoImpl (val context: Context){
    private val fanService by lazy { AnimeApi.animeRetrofit }

    private val animeDao = AnimeDB.getDatabaseInstance(context).animeDao()

     suspend fun getAnime(): Resource<List<AnimeData>>? = withContext(Dispatchers.IO) {
         return@withContext try {
             val response = fanService.getAnime()
             val dbInfo = animeDao.getAnimes()
             if (response.isSuccessful && response.body()!!.getAnimeList.isNotEmpty()) {
                 Resource.Success(response.body()!!.getAnimeList)
             } else {
                 Resource.Success(dbInfo)
             }
         } catch (e: Exception) {
             e.localizedMessage?.let { Resource.Error(it) }
         }
    }

    suspend fun getAnimeByID(id: String) : Resource<List<AnimeData>>? = withContext(Dispatchers.IO) {
        return@withContext try{
            val response = fanService.getAnimeById(id)
            val dbInfo = animeDao.getAnimes()
            if(response.isSuccessful){
                Resource.Success(response.body()!!.getAnimeList)
            }else{
                Resource.Error("Something went Wrong! ")
            }
        } catch (e: Exception){
            e.localizedMessage?.let { Resource.Error(it) }
        }
    }
}
