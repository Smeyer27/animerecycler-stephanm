package com.alecbrando.AnimeApp.model.models

data class LinksX(
    val related: String,
    val self: String
)