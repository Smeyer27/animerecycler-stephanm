package com.alecbrando.AnimeApp.views.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.alecbrando.AnimeApp.databinding.ListItemBinding
import com.alecbrando.AnimeApp.model.models.AnimeData
import com.alecbrando.AnimeApp.model.models.Attributes
import com.alecbrando.AnimeApp.model.models.Data
import com.bumptech.glide.Glide
import kotlin.reflect.KFunction2

class AnimeAdapter(
    private val itemClicked: KFunction2<String, AnimeData, Any?>
) : RecyclerView.Adapter<AnimeAdapter.AnimeViewHolder>() {
    private lateinit var data: List<AnimeData>

    class AnimeViewHolder(private val binding: ListItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun apply(data: AnimeData) = with(binding) {
            animeTitle.text = data.animeTitle
            animeImage.loadImage(data.animePosterImg)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AnimeViewHolder {
        val binding = ListItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return AnimeViewHolder(binding).apply {
            binding.root.setOnClickListener {
                itemClicked("Clicked", data[adapterPosition])
            }
        }
    }

    override fun onBindViewHolder(holder: AnimeViewHolder, position: Int) {
        val item = data[position]
        holder.apply(item)
    }

    override fun getItemCount(): Int {
        return data.size
    }
    fun applyItems(items: List<AnimeData>) {
        data = items
    }
}
fun ImageView.loadImage(url: String) {
    Glide.with(context).load(url).into(this)
}
