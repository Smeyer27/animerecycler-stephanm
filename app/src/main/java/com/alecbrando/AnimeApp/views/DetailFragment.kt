package com.alecbrando.AnimeApp.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.navArgs
import com.alecbrando.AnimeApp.databinding.FragmentDetailBinding
import com.alecbrando.AnimeApp.viewmodels.AnimeDetailViewModel
import com.alecbrando.AnimeApp.views.adapter.loadImage

class DetailFragment: Fragment() {
    // Image View
    // Rating
    // name
    // Description
    private var _binding: FragmentDetailBinding? = null
    private val binding: FragmentDetailBinding get() = _binding!!
    private val args by navArgs<DetailFragmentArgs>()
    private val viewmodel by activityViewModels<AnimeDetailViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    )= FragmentDetailBinding.inflate(inflater, container, false).also {
        _binding = it
        viewmodel.getAnimeDetailsById(args.id)
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    private fun initViews() = with(binding) {
        animeBanner.loadImage(args.anime.attributes.coverImage.large)
        animeDetailTitle.text = args.anime.attributes.canonicalTitle
        animeRating.text = args.anime.attributes.averageRating
        animeDescription.text = args.anime.attributes.synopsis
        animeAgeRating.text = args.anime.attributes.ageRating
        animeSubtype.text = args.anime.attributes.subtype.uppercase()
        if (args.anime.attributes.subtype == "TV") {
            when (args.anime.attributes.status) {
                "current" -> {
                    episodeCountHeader.text = ""
                    animeEpisodeCount.text = ""
                    //                animeNextAirdate.text = args.anime.attributes.nextRelease
                }
                "finished" -> {
                    animeAirdateHeader.text = ""
                    animeNextAirdate.text = ""
                    //                animeEpisodeCount.text = args.anime.attributes.episodeCount
                }
            }
        }
        else {
            animeAirdateHeader.text = ""
            animeNextAirdate.text = ""
            episodeCountHeader.text = ""
            animeEpisodeCount.text = ""

        }
    }
}

