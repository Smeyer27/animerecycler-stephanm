package com.alecbrando.AnimeApp.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.alecbrando.AnimeApp.databinding.FragmentListBinding
import com.alecbrando.AnimeApp.model.models.Animes
import com.alecbrando.AnimeApp.model.models.Data
import com.alecbrando.AnimeApp.model.repo.AnimeRepoImpl
import com.alecbrando.AnimeApp.util.Resource
import com.alecbrando.AnimeApp.util.getJsonDataFromAsset
import com.alecbrando.AnimeApp.viewmodels.AnimeListViewModel
import com.alecbrando.AnimeApp.viewmodels.factory.AnimeViewModelFactory
import com.alecbrando.AnimeApp.views.adapter.AnimeAdapter
import com.google.gson.Gson
import kotlinx.coroutines.flow.collectLatest

class ListFragment(title: Any) : Fragment() {

    private var _binding: FragmentListBinding? = null
    private val binding: FragmentListBinding get() = _binding!!
    private val repo by lazy { AnimeRepoImpl(requireContext()) }
    private val viewModel by activityViewModels<AnimeListViewModel>() { AnimeViewModelFactory(repo) }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentListBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initObservers()
    }

    private fun initObservers() = with(binding) {
        recyclerView.layoutManager = LinearLayoutManager(requireContext())
        with(viewModel) {
            lifecycleScope.launchWhenCreated {
                anime.collectLatest { viewState ->
                    when (viewState) {
                        is Resource.Loading -> {
                            progCirc.visibility = View.VISIBLE
                        }
                        is Resource.Error ->
                        {

                        }
                        is Resource.Success -> {
                            binding.progCirc.visibility = View.GONE
                            recyclerView.adapter = AnimeAdapter(::itemClicked).apply {
                                applyItems(viewState.data)
                            }
                        }
                    }
                }
            }
        }
//        anime.observe(viewLifecycleOwner) { viewState ->
//            when(viewState) {
//                is Resource.Error -> {}
//                is Resource.Loading -> {
//                    binding.progCirc.visibility = View.VISIBLE
//                }
//                is Resource.Success -> {
//                    binding.progCirc.visibility = View.GONE
//                    binding.recyclerView.layoutManager = LinearLayoutManager(requireContext())
//                    binding.recyclerView.adapter = AnimeAdapter(::itemClicked).apply {
//                        applyItems(viewState.data)
//                    }
//                }
//            }
//        }
    }
    private fun itemClicked(title : String, info : Data) {
        val dataDetail = getJsonDataFromAsset(requireContext(), title.toString())
        val actualData = Gson().fromJson(dataDetail, Animes::class.java)
        val action = ListFragmentDirections.actionListFragmentToDetailFragment(id = title, anime = info)
        findNavController().navigate(action)
    }

}